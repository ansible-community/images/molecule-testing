FROM docker:24.0.4-dind

RUN apk add -v --no-cache python3 && apk add -v --no-cache python3-dev py3-pip gcc git curl build-base autoconf automake py3-cryptography linux-headers musl-dev libffi-dev openssl-dev openssh
RUN python3 -m pip install ansible molecule[docker] molecule-plugins[docker] yamllint ansible-lint
