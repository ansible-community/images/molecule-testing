# molecule-testing

Image with molecule, yamllint, ansible-lint. Docker in docker enabled.


Starting with base image version 20.10.14-dind, TLS must explicitely disabled like this (check image tag, this is only an example):

```
services:
  - name: registry.ethz.ch/ansible-community/images/molecule-testing:24.0.4
    command: ["--tls=false"]

```
